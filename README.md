# devops

## Este projeto foi feito com:

* [Python 3.8.9](https://www.python.org/)
* [Flask 2.1.0](https://flask.palletsprojects.com/en/1.1.x/)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.

```
git clone https://gitlab.com/lbnery/devops.git
cd devops
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python3 contrib/env_gen.py
python3 app.py
```

```python
# POST /store data: {name:}
# GET /store/<string:name>
# GET /store
# POST /store/<string:name>/item {name:, price:}
# GET /store/<string:name>/item
```

## Chamando a API com Postman


```
POST
http://localhost:5000/store
Content-Type:application/json

{
    "name": "Uma loja ruim"
}
```

```
POST
http://localhost:5000/store/Minha Loja Legal/item
Content-Type:application/json

{
    "name": "Um item",
    "price": 10.99
}
```

```
POST
http://localhost:5000/item/<name>
Content-Type:application/json

{
    "price": 12.99
}
```